package com.mybooksystem.springboot.javaBase;

/**
 * 单例设计模式。[饿汉式、懒汉式]
 */
// 饿汉式
//public class Singleton {
//    // 1.私有化构造器
//    private Singleton(){
//    }
//    // 2.内部提供一个当前类的实例。
//    // 4.次实例也必须静态化。
//    private static Singleton single = new Singleton();
//    // 3.提供公共的静态的方法，返回当前类的对象。
//    public static Singleton getInstance(){
//        return single;
//    }
//}

// 懒汉式
public class Singleton {
    // 1.私有化构造器
    private Singleton(){
    }
    // 2.内部提供一个当前类的实例。
    // 4.次实例也必须静态化。
    private static Singleton single;
    // 3.提供公共的静态的方法，返回当前类的对象。
    public static Singleton getInstance(){
        if (single == null){
            single = new Singleton();
        }
        return single;
    }
}
