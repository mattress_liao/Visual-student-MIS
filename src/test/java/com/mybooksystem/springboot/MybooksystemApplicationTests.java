package com.mybooksystem.springboot;

import com.mybooksystem.springboot.Dao.StudentInfoMapper;
import com.mybooksystem.springboot.controller.StudentsControllerTest;
import com.mybooksystem.springboot.interceptor.EncodingInterceptor;
import com.mybooksystem.springboot.pojo.StudentAndSubjectScore;
import com.mybooksystem.springboot.pojo.student.Students;
import com.mybooksystem.springboot.service.StudentsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;
import java.util.Map;

@SpringBootTest
class MybooksystemApplicationTests {



    @Autowired
    private StudentsService studentsService;

    @Autowired
    private StudentInfoMapper studentInfoMapper;


    @Test
    void testStudentsControllerTest(){
        new StudentsControllerTest();
    }
    @Test
    void contextLoads() {

        // 删除测试。结果都返回 0。
//        System.out.println("结果（存在的数据）：" + studentsService.deleteStudentInfo("14554545455"));

//        System.out.println("结果（不存在的数据）：" + studentsService.deleteStudentInfo("25151151154"));
    }

    @Test
    public void testAddStudents(){
//        studentsService.addStudentInfo(new Students("1924010223","赵七","454241212021021202",1,
////                "2021-09-18"
//                new Date()
//                ,"高考","全日制","本科",4,
//                "党员","广东工程职业技术学院"));
    }

    @Test
    public void testStudentAllSubjectsScore(){
//        List<Map<String,String>> studentAllSubjectsScore = studentInfoMapper.getStudentAllSubjectsScore(String.valueOf(100001));
//        System.out.println("获取课程名和分数 =======================");
//        studentAllSubjectsScore.forEach(System.out::println);


//        List<StudentAndSubjectScore> studentInfoListForScore = studentsService.getStudentInfoListForScore(100);
//        studentInfoListForScore.forEach(System.out::println);
    }

    @Test
    public void testDemo(){

        char[] arr = new char[]{'a','b','c'};
        System.out.println(arr); // 内容，因为sout传char时，用char[]接收。
        int[] arr1 = new int[]{1,2,3};
        System.out.println(arr1);// 地址
        double[] arr2 = new double[]{1.1,2.2,3.2};
        System.out.println(arr2);// 地址
    }

    @Test
    public void demo(){
        Object o1 = true?new Integer(1):new Double(2.0);
        System.out.println(o1);

        Object o2 ;
        if (true)
            o2 = new Integer(1);
        else
            o2 = new Double(2.0);
        System.out.println(o2);
    }

    @Test
    public void method1(){
        Integer i = new Integer(1);
        Integer j = new Integer(1);
        System.out.println(i == j);

        Integer m = 1;
        Integer n = 1;
        System.out.println(m == n);

        Integer x = 128;
        Integer y = 128;
        System.out.println(x == y);
    }

    @Test
    public void testAddOne(){
        String str = "24190102";
        String substring1 = str.substring(0, 2);
        String substring2 = str.substring(2);
        System.out.println("切割数组1：" + substring1);
        System.out.println("切割数组2：" + substring2);
//        System.out.println(addOne(0));
    }
//    int addOne(final int x){
//        return ++x;
//    }
}
