package com.mybooksystem.springboot.controller;

import com.mybooksystem.springboot.service.StudentsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * StudentService 的功能性测试类。
 */
@SpringBootTest
public class StudentServiceTest {

    @Autowired
    private StudentsService studentsService;
    /**
     * 测试
     * 根据 班级编号 获取前 n 条学生分数信息的列表。
     */
    @Test
    public void test(){
        System.out.println("===================================================");
//        studentsService.getStudentInfoListForScore("24210103",100);
        System.out.println("===================================================");
    }
}
