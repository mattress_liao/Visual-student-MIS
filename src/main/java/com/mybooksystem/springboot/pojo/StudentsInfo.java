package com.mybooksystem.springboot.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 学生信息的 实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Deprecated
public class StudentsInfo {

    /**
     * 学号
     */
    private String studentId;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String gender;
    /**
     * 语文
     */
    private Float Chinese;
    /**
     * 数学
     */
    private Float Math;
    /**
     * 英语
     */
    private Float English;
    /**
     * 总分
     */
    private Float totalGrade;
}
