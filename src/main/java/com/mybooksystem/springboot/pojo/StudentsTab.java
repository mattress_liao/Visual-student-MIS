package com.mybooksystem.springboot.pojo;

import java.util.List;

/**
 * 学生表格的 实体类.(感觉有点多余。)
 */
@Deprecated
public class StudentsTab<StudentsInfo> {

    /**
     * 学生表格的 列表
     */
    private List<StudentsInfo> studentsInfoList;
    /**
     * 显示的学生信息行数.(默认是十行)
     */
    private Integer showRows = 10;

    public StudentsTab() {
    }

    public StudentsTab(List<StudentsInfo> studentsInfoList, Integer showRows) {
        this.studentsInfoList = studentsInfoList;
        this.showRows = showRows;
    }



    public List<StudentsInfo> getStudentsInfoList() {
        return studentsInfoList;
    }

    public void setStudentsInfoList(List<StudentsInfo> studentsInfoList) {
        this.studentsInfoList = studentsInfoList;
    }

    public Integer getShowRows() {
        return showRows;
    }

    public void setShowRows(Integer showRows) {
        this.showRows = showRows;
    }


    @Override
    public String toString() {
        return "StudentsTab{" +
                "studentsInfoList=" + studentsInfoList +
                ", showRows=" + showRows +
                '}';
    }


    /**
     * 获取 学生信息表 的函数。
     * @return
     */
    public List<StudentsInfo> getStudentInfoList(){
        return null;
    }
}
