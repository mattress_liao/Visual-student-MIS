package com.mybooksystem.springboot.pojo;

/**
 * 与数据库 students 表格相对应的 JavaBean。
 */
@Deprecated
public class Students {
    private String studentId;
    private String stuName;
    private Integer age;
    private Integer gender;

    public Students() {
    }

    public Students(String studentId, String stuName) {
        this.studentId = studentId;
        this.stuName = stuName;
    }

    public Students(String studentId, String stuName, Integer age, Integer gender) {
        this.studentId = studentId;
        this.stuName = stuName;
        this.age = age;
        this.gender = gender;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Students{" +
                "studentId='" + studentId + '\'' +
                ", stuName='" + stuName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }
}
