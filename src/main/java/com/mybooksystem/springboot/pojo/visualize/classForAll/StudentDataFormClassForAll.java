package com.mybooksystem.springboot.pojo.visualize.classForAll;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 单个专业的可视化的 学生 数据部分。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentDataFormClassForAll {
    /**
     * 学生学号
     */
    private String studentId;
    /**
     * 学生姓名
     */
    private String studentName;
    /**
     * 学生性别
     */
    private Integer gender;
    /**
     * 班级名称
     */
    private String className;
    /**
     * 科目
     */
    private String courseName;
    /**
     * 分数
     */
    private Float subjectsScore;

}
