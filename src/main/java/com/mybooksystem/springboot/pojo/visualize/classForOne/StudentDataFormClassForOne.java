package com.mybooksystem.springboot.pojo.visualize.classForOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 单个班级的可视化的 学生 数据部分。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentDataFormClassForOne {
    /**
     * 学生学号
     */
    private String studentId;
    /**
     * 学生姓名
     */
    private String studentName;
    /**
     * 学生性别
     */
    private Integer gender;
    /**
     * 班级
     */
    private String className;
    /**
     * 科目
     */
    private String courseName;
    /**
     * 分数
     */
    private Float subjectsScore;

}
