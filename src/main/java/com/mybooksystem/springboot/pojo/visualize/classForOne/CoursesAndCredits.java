package com.mybooksystem.springboot.pojo.visualize.classForOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 分数列表 和 学分列表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CoursesAndCredits {
    /**
     * 科目名称
     */
    private String courseName;
    /**
     * 学分
     */
    private Float credits;
}
