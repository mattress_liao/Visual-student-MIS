package com.mybooksystem.springboot.pojo.student;

import lombok.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 学生个人基础信息实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//@ToString
public class Students {

    // 日期格式化对象
    // 使用 static 之后，不会被 @Data 注解自动生成 getter/setter 方法。
    private static final SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 学号
     */
    private String studentId;
    /**
     * 学生姓名
     */
    private String studentName;
    /**
     * 学生身份证
     */
    private String idCard;
    /**
     * 学生班级
     */
    private String className;
    /**
     * 学生性别
     */
    private Integer gender;
    /**
     * 入学时间
     */
    private Date admissionDate;
    /**
     * 入学类型。（高考、学考、3+2、成考）
     */
    private String admissionType;
    /**
     * 就读类型。（全日制、非全日制）
     */
    private String studyType;
    /**
     * 学历（教育程度【最高学历】）
     */
    private String educationDegree;
    /**
     * 就读年限
     */
    private Integer studyYear;
    /**
     * 政治面貌
     */
    private String politicalStatus;
    /**
     * 家庭地址
     */
    private String address;

    public String getAdmissionDate() {
//        return admissionDate;
        return ft.format(admissionDate);
    }

    /**
     * 添加学生信息时，会出现数据类型转换失败的情况。
     * @return
     */
    /*
    public String getGender() {
        return gender==1 ? "男" : "女" ;
    }
    */

    @Override
    public String toString() {
        return "Students{" +
                "studentId='" + studentId + '\'' +
                ", studentName='" + studentName + '\'' +
                ", idCard='" + idCard + '\'' +
                ", className='" + className + '\'' +
                ", gender=" + gender +
                ", admissionDate=" + admissionDate +
                ", admissionType='" + admissionType + '\'' +
                ", studyType='" + studyType + '\'' +
                ", studyYear=" + studyYear +
                ", educationDegree='" + educationDegree + '\'' +
                ", politicalStatus='" + politicalStatus + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
