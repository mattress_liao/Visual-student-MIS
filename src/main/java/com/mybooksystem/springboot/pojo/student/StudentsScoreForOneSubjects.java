package com.mybooksystem.springboot.pojo.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 学生单科成绩实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentsScoreForOneSubjects {

    // 日期格式化对象
    // 使用 static 之后，不会被 @Data 注解自动生成 getter/setter 方法。
    private static final SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 学号
     */
    private String studentId;
    /**
     * 课程号
     */
    private String courseID;
//    private String secondCollegeId;
//    private String proId;
    /**
     * 学生班级编号
     */
    private String classId;
    /**
     * 录入日期
     */
    private Date entryDate;
    /**
     * 单科科目的成绩
     */
    private Float subjectsScore;

    public String getEntryDate() {
        return ft.format(entryDate);
    }
}
