package com.mybooksystem.springboot.pojo.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 学生总成绩分数情况信息 实体类。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentsTotalScoreInfo {
    /**
     * 学号
     */
    private String studentId;
    /**
     * 学生班级编号
     */
    private String classId;
    /**
     * 总成绩
     */
    private Float totalScore;
    /**
     * 最高成绩
     */
    private Float highestScore;
    /**
     * 最低成绩
     */
    private Float minimumScore;
    /**
     * 最高成绩课程号
     */
    private String highestScoreCourseID;
    /**
     * 最低成绩课程号
     */
    private String minimumScoreCourseID;
    /**
     * 平均成绩
     */
    private Float averageScore;
}
