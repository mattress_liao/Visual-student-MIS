package com.mybooksystem.springboot.pojo.subjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 课程名和课程分数的实体类。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClassNameAndSubjectsScore {
    /**
     * 课程名
     */
    private String courseName;
    /**
     * 分数
     */
    private Float subjectsScore;
}
