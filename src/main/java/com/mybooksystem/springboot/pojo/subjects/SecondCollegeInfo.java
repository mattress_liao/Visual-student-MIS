package com.mybooksystem.springboot.pojo.subjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 二级学院信息的实体类。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SecondCollegeInfo {
    /**
     * 二级学院名称
     */
    private String secondCollegeName;
    /**
     * 专业编号
     */
    private String proId;
    /**
     * 专业名称
     */
    private String proName;
    /**
     * 就读年级
     */
    private String year;
    /**
     * 班级编号
     */
    private String classId;
    /**
     * 班级姓名
     */
    private String className;
}
