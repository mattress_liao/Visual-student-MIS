package com.mybooksystem.springboot.pojo.subjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 班级课程信息 实体类。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClassSubjectsInfo {
    /**
     * 班级编号
     */
    private String classId;
    /**
     * 班级名称
     */
    private String className;
    /**
     * 课程号
     */
    private String courseID;
    /**
     * 课程名称
     */
    private String courseName;
}
