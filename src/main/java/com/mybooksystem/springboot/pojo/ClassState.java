package com.mybooksystem.springboot.pojo;

/**
 * 班级状态实体类
 */
public class ClassState {
    /**
     * 班级总人数
     */
    private Integer classNum;
    /**
     * 班级全科目及格人数
     */
    private Integer classPassNum;
    /**
     * 及格率
     */
    private Float classPassPercentage;

    /**
     * 科目总数
     */
    private Integer subjectsNumbers;

    public ClassState() {
    }

    public ClassState(Integer classNum, Integer classPassNum, Float classPassPercentage, Integer subjectsNumbers) {
        this.classNum = classNum;
        this.classPassNum = classPassNum;
        this.classPassPercentage = classPassPercentage;
        this.subjectsNumbers = subjectsNumbers;
    }

    public Integer getClassNum() {
        return classNum;
    }

    public void setClassNum(Integer classNum) {
        this.classNum = classNum;
    }

    public Integer getClassPassNum() {
        return classPassNum;
    }

    public void setClassPassNum(Integer classPassNum) {
        this.classPassNum = classPassNum;
    }

    public Float getClassPassPercentage() {
        return classPassPercentage;
    }

    public void setClassPassPercentage(Float classPassPercentage) {
        this.classPassPercentage = classPassPercentage;
    }

    public Integer getSubjectsNumbers() {
        return subjectsNumbers;
    }

    public void setSubjectsNumbers(Integer subjectsNumbers) {
        this.subjectsNumbers = subjectsNumbers;
    }

    @Override
    public String toString() {
        return "ClassState{" +
                "classNum=" + classNum +
                ", classPassNum=" + classPassNum +
                ", classPassPercentage=" + classPassPercentage +
                ", subjectsNumbers=" + subjectsNumbers +
                '}';
    }
}
