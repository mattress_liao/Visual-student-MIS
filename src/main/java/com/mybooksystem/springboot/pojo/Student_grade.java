package com.mybooksystem.springboot.pojo;

/**
 * 学生成绩表 的 JavaBean。
 */
@Deprecated
public class Student_grade {

    private String studentId;
    private Float chinese;
    private Float math;
    private Float english;
    private Float totalGrade;

    public Student_grade() {
    }

    public Student_grade(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Float getChinese() {
        return chinese;
    }

    public void setChinese(Float chinese) {
        this.chinese = chinese;
    }

    public Float getMath() {
        return math;
    }

    public void setMath(Float math) {
        this.math = math;
    }

    public Float getEnglish() {
        return english;
    }

    public void setEnglish(Float english) {
        this.english = english;
    }

    public Float getTotalGrade() {
        return totalGrade;
    }

    public void setTotalGrade(Float totalGrade) {
        this.totalGrade = totalGrade;
    }

    @Override
    public String toString() {
        return "Student_grade{" +
                "studentId='" + studentId + '\'' +
                ", chinese=" + chinese +
                ", math=" + math +
                ", english=" + english +
                ", totalGrade=" + totalGrade +
                '}';
    }
}
