package com.mybooksystem.springboot.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 学生成绩信息表。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentAndSubjectScore {
    /**
     * 学号
     */
    private String studentId;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 班级名称
     */
    private String className;
    /**
     * 课程和分数。
     * <课程名,分数 >
     */
    private List<Float> subjectsScore;


//    private List<Map<String,String>> subjectsScore;
//    private Float subjectsScore1;
//    private Float subjectsScore2;
//    private Float subjectsScore3;
//    private Float subjectsScore4;
//    private Float subjectsScore5;
//    private Float subjectsScore6;
//    private Float subjectsScore7;
//    private Float subjectsScore8;

    public StudentAndSubjectScore(String studentId, String studentName, String className) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.className = className;
    }

    /**
     * 把当前成绩表的属性按照
     *      【学号, 姓名, 班级名称, 课程, 课程1, 课程2, 课程3, 课程4, 课程5, 课程6, 课程7】
     *      进行数组化。
     * @return
     *      返回数组化的 学生成绩表信息【单个学生】。
     */
    public List<Object> toList(){

        List<Object> list = new ArrayList<>();
        list.add(this.getStudentId());
        list.add(this.getStudentName());
        list.add(this.getClassName());
        list.addAll(subjectsScore);
//        list.add(this.getSubjectsScore1());
//        list.add(this.getSubjectsScore2());
//        list.add(this.getSubjectsScore3());
//        list.add(this.getSubjectsScore4());
//        list.add(this.getSubjectsScore5());
//        list.add(this.getSubjectsScore6());
//        list.add(this.getSubjectsScore7());
//        list.add(this.getSubjectsScore8());
        return list;
    }
    //    @Override
//    public String toString() {
//        return "StudentAndSubjectScore{" +
//                "studentId='" + studentId + '\'' +
//                ", studentName='" + studentName + '\'' +
//                ", className='" + className + '\'' +
//                ", subjectsScore=" + subjectsScore +
//                '}';
//    }
}
