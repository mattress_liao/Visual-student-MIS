package com.mybooksystem.springboot.pojo.manage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * 管理员 实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Administrators {

    /**
     * 用户名对应的唯一id .
     */
    private Integer uid;
    /**
     * 管理员用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 登入时间。（最近一次登录时间）
     */
    private Date loginDate;
    /**
     * 登出时间，（最近一次）
     */
    private Date logoutDate;
    /**
     * 登录次数
     */
    private Integer loginNum;
}
