package com.mybooksystem.springboot.pojo.manage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * 与登入登出有关的实体类。<br>
 * 记录登入登出的所有时间。<br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginDateForMySMS {

    /**
     * 用户名对应的唯一id .
     */
    private Integer uid;

    /**
     * 登入时间
     */
    private Date loginDate;

    /**
     * 登出时间
     */
    private Date logoutDate;

    /**
     * 系统信息。（登录的设备信息）
     */
    private String systemInfo;
}
