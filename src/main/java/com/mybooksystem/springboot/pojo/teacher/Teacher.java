package com.mybooksystem.springboot.pojo.teacher;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * 教师基本信息实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Teacher {

    /**
     * 教师对应全表中的唯一id
     */
    private Integer uid;
    /**
     * 教师姓名
     */
    private String teacherName;
    /**
     * 教师身份证
     */
    private String IDCard;
    /**
     * 教师性别
     */
    private Integer Gender;
    /**
     * 教师年龄
     */
    private Integer age;

    /**
     * 开始任职的时间
     */
    private Date startWorkingTime;
    /**
     * 任职的类型。
     * 取值有 全职、外教、兼职。
     */
    private String WorkingType;
    /**
     * 任职的科目
     * 取值有 .....
     */
    private String WorkingSubjects;
    /**
     * 任职年限
     */
    private Integer WorkingYear;
    /**
     * 学历（教育程度【最高学历】）
     */
    private String educationDegree;
    /**
     * 毕业的院校
     */
    private String graduateSchool;
    /**
     * 教师综合评价
     */
    private String evaluation;

}
