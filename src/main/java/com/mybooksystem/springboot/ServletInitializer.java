package com.mybooksystem.springboot;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
 * web容器中进行部署。本项目中不投入使用。
 *
 */
@Deprecated
 public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        System.out.println("spring boot 项目 部署 war 包！");
        return application.sources(MybooksystemApplication.class);
    }

}
