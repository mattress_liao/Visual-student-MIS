package com.mybooksystem.springboot.Dao;

import com.mybooksystem.springboot.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * 登录相关的 dao 层。
 */
@Mapper     //springboot 2.0 新写法。
@Repository //加上该注解就不会出现 Could not autowire. No beans of 'UserDao' type found.
public interface LoginDao {

    @Select("select username from administrators where username=#{username}")
    public String isUsername(String username);

    @Select("select username,password from administrators where username=#{username}")
    User loginState(String username);
}
