package com.mybooksystem.springboot.Dao;

import com.mybooksystem.springboot.pojo.subjects.SecondCollegeInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 获取二级学院信息 dao 层。
 */
@Repository
@Mapper
public interface SecondCollegeMapper {

    /**
     * 通过数据库动态视图。获取该学校内的所有的二级学院、专业、年级、班级信息。
     * @return
     *      查询到的数据视图对象。
     */
    @Select("SELECT * FROM college_info_view")
    List<SecondCollegeInfo> getSecondCollegeAndProClassSituationInfo();

}
