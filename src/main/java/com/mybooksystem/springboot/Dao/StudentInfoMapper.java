package com.mybooksystem.springboot.Dao;

import com.mybooksystem.springboot.pojo.student.Students;
import com.mybooksystem.springboot.pojo.student.StudentsScoreForOneSubjects;
import com.mybooksystem.springboot.pojo.subjects.ClassNameAndSubjectsScore;
import com.mybooksystem.springboot.pojo.visualize.classForOne.StudentDataFormClassForOne;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 学生信息的 mapper 。
 */
@Mapper
@Repository
public interface StudentInfoMapper {

    /**
     * 获取表中的前 n 条记录。
     * @param proId：      专业号.
     * @param classId：    班级号.
     * @param startNum:    前几条记录.
     * @return
     */
//    @Select("select * from students LIMIT #{startNum}")
    @Select("select DISTINCT stu.* " +
                "from " +
            "students stu,students_and_subjectscore_relation stu_sub " +
                "WHERE " +
            "stu_sub.proId=#{proId} AND stu_sub.classId=#{classId} AND " +
            "stu.studentId = stu_sub.studentId " +
            "LIMIT #{startNum}")
    public List<Students> getStudentsInfo(String proId,String classId,Integer startNum);

    /**
     * 获取所有学生的信息。
     * @return
     */
    @Select("select * from students")
    public List<Students> getAllStudentsInfo();

    /**
     * 根据学生学号删除学生信息。
     * @param stuId 要删除的学生的学号。
     */
    @Delete("DELETE FROM students WHERE studentId=#{stuId}")
    public void deleteStudentInfo(String stuId);

    /**
     * 添加学生
     * @param students 添加的对象。
     * @return
     *      true：添加成功。
     *      false：添加失败。
     */
    @Insert("INSERT INTO students" +
            "(studentId,studentName,idCard,className,gender,admissionDate,admissionType,studyType,educationDegree,studyYear,politicalStatus,address) " +
            "VALUES" +
            "(#{studentId},#{studentName},#{idCard},#{className},#{gender},#{admissionDate},#{admissionType},#{studyType},#{educationDegree},#{studyYear},#{politicalStatus},#{address})")
    public Boolean addStudentInfo(Students students);

    /**
     * 根据学号进行查询学生信息。
     * @param studentId：需要查询的学生学号。
     * @return
     *      查询到的学生信息。
     *      null：查询不到。
     */
    @Select("select * from students where studentId=#{studentId}")
    public Students getStudentInfo(String studentId);

    /**
     * 判断该学生的学号是否存在学生表中，
     * @param studentId 要进行判断的学号。
     * @return
     *      返回学生学号。
     */
    @Select("select studentId from students where studentId=#{studentId}")
    public String isStudentInfo(String studentId);

    /**
     * 根据参数对象，进行修改学生信息。
     *
     * @param students ：要修改的学生对象。
     * @return
     *      true：修改成功。
     *      false：修改失败。
     */
    @Update("UPDATE students " +
            "SET studentName=#{studentName},idCard=#{idCard},gender=#{gender}," +
            "admissionDate=#{admissionDate},admissionType=#{admissionType},studyType=#{studyType}," +
            "educationDegree=#{educationDegree} ,studyYear=#{studyYear},politicalStatus=#{politicalStatus}," +
            "address=#{address} " +
            "WHERE studentId=#{studentId}")
    Boolean updateStudentInfo(Students students);









    /**
     * 获取前 n 条学生分数信息的列表。
     * @param startNum：开始的行数
     * @return
     *      返回查询到的学生列表。
     *      null：为未查到。
     */
//    @Select("select studentId from students LIMIT #{startNum}")
    @Deprecated
    List<Object> getStudentInfoListForScore(Integer startNum);

    /**
     * 获取当前学号所对应的学生姓名。
     * @param studentId：要查询的学生学号。
     * @return
     *      查询到的学生姓名。
     */
    @Select("select studentName from students WHERE studentId=#{studentId}")
    String getStudentName(String studentId);

    // 获取前 n 条学生的学号和姓名。
    @Select("select studentId,studentName from students LIMIT #{startNum}")
    List<Map<String,String>> getStudentIdAndStudentName(Integer startNum);



//    -------------------------------
    /**
     * 根据 专业号 和 班级号 获取条学生信息。
     * @param proId：    需要获取的专业号。
     * @param classId：  需要获取的班级号。
     * @return
     *      查询到的学生学号和姓名。
     */
    // 获取前 n 条学生的学号和姓名。
    @Select("select studentId,studentName from students LIMIT #{startNum}")
    List<Map<String,String>> getStudentIdAndStudentNameForClassId(String proId,String classId, Integer startNum);



    /**
     *根据 专业编号 获取 【学号、姓名、性别、班级、科目名、分数】列表。
     *
     * 学生成绩信息 表。
     * @param proId：要获取的 专业编号。
     * @return
     *      返回结果集。
     */
    @Select("SELECT DISTINCT stu.studentId, stu.studentName,stu.gender,class_pro.className, pro.courseName, stu_sub.subjectsScore " +
                "FROM " +
            "students_and_subjectscore_relation stu_sub " +

                    "LEFT JOIN " +
                "students stu " +
                    "ON " +
                "stu.studentId = stu_sub.studentId " +

                        "LEFT JOIN " +
                    "pro_and_course_relation pro " +
                        "ON " +
                    "pro.courseID = stu_sub.courseID AND pro.proId = stu_sub.proId " +

                            "LEFT JOIN " +
                        "class_and_pro_relation class_pro " +
                            "ON " +
                        "stu_sub.proId = class_pro.proId AND stu_sub.classId = class_pro.classId " +
                "WHERE " +
            "stu_sub.proId = #{proId} AND stu_sub.classId = #{classId}")
    List<StudentDataFormClassForOne> getStudentScoreInfo(String proId, String classId);


    // 根据 学号，查询当前学生的全部课程号课程名的分数。
    @Select("select " +
            "courseName,subjectsScore " +
//            "subjectsScore " +
            "from " +
            "students_and_subjectscore_relation stuInfo,pro_and_course_relation peoInfo " +
            "where " +
            "studentId=#{studentId} AND stuInfo.courseID=peoInfo.courseID")
    List<ClassNameAndSubjectsScore> getStudentAllSubjectsScore(String studentId);






    /**
     * 根据学号进行修改学生成绩信息。
     * @param studentId：学生学号。
     * @param courseId：课程号。
     * @param subjectsScore：分数。
     */
    @Update("UPDATE students_and_subjectscore_relation " +
            "SET " +
                "subjectsScore=#{subjectsScore} " +
            "WHERE " +
                "studentId=#{studentId} AND courseID=#{courseId}")
    Integer updateStudentScoreInfo(String studentId,String courseId,Float subjectsScore);



    /**
     * 根据 课程名称查询课程号。
     * @param courseName：课程名
     * @return
     *      返回课程号。
     */
    @Select("SELECT courseID FROM pro_and_course_relation WHERE courseName=#{courseName} AND proId=#{proId}")
    String getCourseID(String proId,String courseName);
    /**
     * 根据 课程号 查询 课程名。
     * @param courseId：要查询的课程号。
     * @return
     *      查询到的课程名。
     */
    @Select("SELECT courseName FROM pro_and_course_relation WHERE courseId=#{courseId}")
    String getCourseName(String courseId);
    /**
     * 根据 班级编号 获取 课程名
     *
     * 排序方式：默认按     课程号 进行升序排序。
     *
     * @param proId：班级编号
     * @return
     *      查询到的课程号。
     */
    @Select("SELECT courseName FROM pro_and_course_relation WHERE proId=#{proId}")
    List<String> getCourseNameList(String proId);

    /**
     * 根据 班级名称 获取 课程id。
     * @param proName：班级名称
     * @return
     *      返回查询到的结果。
     */
    @Select("SELECT courseId FROM pro_and_course_relation WHERE proName=#{proName}")
    List<String> getCourseIdList(String proName);


    @Select("SELECT proId FROM class_and_pro_relation WHERE className=#{className}  LIMIT 1")
    String getClassId(String className);
    /**
     * 根据班级编号，获取班级名称。
     * @param proId：要获取的班级编号。
     * @return
     *      查询到的班级名称。
     */
    @Select("SELECT proName FROM pro_and_course_relation WHERE proId=#{proId}")
    String getClassName(String proId);

    /**
     * 添加学生成绩表。单科
     * @param studentScoreInfo：学生成绩表信息。
     */
    @Insert("INSERT INTO students_and_subjectscore_relation" +
                "(studentId,courseID,secondCollegeId,proId,classId,entryDate,subjectsScore) " +
            "VALUES" +
                "(#{studentId},#{courseID},#{secondCollegeId},#{proId},#{classId},#{entryDate},#{subjectsScore})")
    void addStudentScoreInfo(StudentsScoreForOneSubjects studentScoreInfo);

    /**
     * 根据 课程id 查询是否存在。
     * @param courseName：课程名。
     * @return
     *      查询到的结果。
     */
    @Select("select courseID from pro_and_course_relation where courseName=#{courseName}")
    String isCourseID(String courseName);


    /**
     * 根据 专业号 查询该专业的科目个数。
     * @param proId：专业号。
     * @return
     *      返回该专业的科目数。
     */
    @Select("SELECT count(1) FROM pro_and_course_relation WHERE proId = #{proId}")
    Integer getClassNumber(String proId);
}
