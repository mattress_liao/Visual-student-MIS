package com.mybooksystem.springboot.Dao;

import com.mybooksystem.springboot.pojo.ClassState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 班级状态 dao 层。
 */
@Mapper     //springboot 2.0 新写法。
@Repository //加上该注解就不会出现 Could not autowire. No beans of 'UserDao' type found.
public interface ClassStateDao {


    /**
     * 查询学生总人数。
     * @return
     */
    @Select("SELECT COUNT(*) FROM students")
    public Integer findClassNum();

    /**
     *
     * 班级全科目及格人数
     * @return
     */
    @Select("SELECT COUNT(*) FROM student_grade WHERE chinese>=60 AND math>=60 AND english>=60")
    Integer findClassPassNum();

    /**
     * 及格率 (不归 dao 层所管)
     * @return
     */
//    @Select("")
//    Float findClassPassPercentage();

    /**
     * 科目总数
     * @return
     */
    @Select("SELECT COUNT(*) FROM subjects_number")
    Integer findSubjectsNumbers();
}
