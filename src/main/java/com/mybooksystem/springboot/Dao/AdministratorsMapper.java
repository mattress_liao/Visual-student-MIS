package com.mybooksystem.springboot.Dao;

import com.mybooksystem.springboot.pojo.manage.Administrators;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * 管理员相关的 dao 层。
 */
@Mapper
@Repository
public interface AdministratorsMapper {


    /**
     * 获取管理员信息。
     * @param username
     * @return
     */

    @Select("SELECT uid,username,`password` FROM administrators where username=#{username}")
//    @Select("select * from administrators")
    Administrators getAdministratorInfo(String username);

    @Update("UPDATE administrators " +
            "SET " +
                "username=#{username},`password`=#{password} WHERE uid=#{uid}")
    Boolean updateAdminInfo(Administrators administrators);
}
