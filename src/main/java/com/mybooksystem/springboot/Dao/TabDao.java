package com.mybooksystem.springboot.Dao;

import com.mybooksystem.springboot.pojo.Student_grade;
import com.mybooksystem.springboot.pojo.StudentsInfo;
import com.mybooksystem.springboot.pojo.student.Students;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与表格信息相关的 dao 层。
 */
@Mapper     //springboot 2.0 新写法。
@Repository //加上该注解就不会出现 Could not autowire. No beans of 'UserDao' type found.
public interface TabDao {

    /**
     * students 信息的表格。
     * @param
     * @param lastLineData
     * @return
     */
    @Select("SELECT * FROM students LIMIT #{lastLineData}")
    public List<Students> getStudentsTab(Integer lastLineData);

    /**
     * Student_grade 信息的表格。
     * @param lastLineData
     * @return
     */
    @Select("select * from student_grade LIMIT #{lastLineData}")
    public List<Student_grade> getStudent_gradeTab(Integer lastLineData);

//    @Select("SELECT stu.*, stug.chinese,stug.math,stug.english,stug.totalGrade FROM students stu, student_grade stug WHERE stug.studentId = stu.studentId LIMIT #{lastLineData}")
    /**
     * 查询学生基本信息.查询前 n 行数据。
     *
     * @param lastLineData 最后一行。取值范围为 左开右闭 闭区间。
     * @return
     *
     */
    @Select("select * from Students LIMIT #{lastLineData}")
    public List<Students> getStudentsInfo(Integer lastLineData);
}
