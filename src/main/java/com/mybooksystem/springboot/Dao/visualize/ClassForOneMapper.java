package com.mybooksystem.springboot.Dao.visualize;

import com.mybooksystem.springboot.pojo.visualize.classForOne.CoursesAndCredits;
import com.mybooksystem.springboot.pojo.visualize.classForOne.StudentDataFormClassForOne;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 单个班级的可视化表格数据 [mapper 层]
 */
@Repository
@Mapper
public interface ClassForOneMapper {

    /**
     *根据 专业编号 和 班级号 获取 【学号、姓名、性别、科目名、分数】列表。
     * @param proId：要获取的 专业编号。
     * @param classId：要获取的 班级编号。
     * @return
     *      返回结果集。
     */
    @Select("SELECT DISTINCT stu.studentId, stu.studentName,stu.gender, pro.courseName, stu_sub.subjectsScore " +
                "FROM " +
            "students_and_subjectscore_relation stu_sub " +
                    "LEFT JOIN " +
                "students stu " +
                    "ON " +
                "stu.studentId = stu_sub.studentId " +
                        "LEFT JOIN " +
                    "pro_and_course_relation pro " +
                        "ON " +
                    "pro.courseID = stu_sub.courseID " +
                "WHERE " +
            "stu_sub.proId = #{proId} AND classId = #{classId} ")
    List<StudentDataFormClassForOne> getData(String proId, String classId);

    /**
     * 根据 专业编号 获取 【科目名】列表。
     * @param proId：专业编号。
     * @return
     *      返回结果集。
     */
    @Select("SELECT courseName, credits " +
                "FROM " +
            "pro_and_course_relation " +
                "WHERE " +
            "proId = #{proId}")
    List<CoursesAndCredits> getCourseListAndCredits(String proId);
}
