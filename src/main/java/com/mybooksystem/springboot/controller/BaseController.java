package com.mybooksystem.springboot.controller;

import com.mybooksystem.springboot.util.DesUtils;
import org.springframework.stereotype.Controller;

/**
 * 所有控制器的 基础父类
 */
public class BaseController {

    /**
     * 唯一标识。
     */
    static String uuid ;
}
