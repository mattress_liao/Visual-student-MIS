package com.mybooksystem.springboot.controller;

import com.mybooksystem.springboot.config.Result;
import com.mybooksystem.springboot.pojo.subjects.SecondCollegeInfo;
import com.mybooksystem.springboot.service.SecondCollegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 二级学院的控制器层
 */
@RestController
public class SecondCollegeController {

    /**
     *
     */
    @Autowired
    private SecondCollegeService secondCollegeServer;
    /**
     * 获取 “广东工程职业技术学院” 的二级学院、专业、年级、和班级信息。
     * @return
     *      返回 “广东工程职业技术学院” 的 二级学院、专业、年级、和班级信息。
     */
    @GetMapping("/managementSystem/secondCollegeAndProClassSituation")
    public Result<List<SecondCollegeInfo>> demo(){
        // 获取二级学院的信息。
        List<SecondCollegeInfo> collegeInfoList = secondCollegeServer.getSecondCollegeAndProClassSituationInfo();

        collegeInfoList.forEach(System.out::println);

        return new Result<>(200,"成功",collegeInfoList);
    }
}
