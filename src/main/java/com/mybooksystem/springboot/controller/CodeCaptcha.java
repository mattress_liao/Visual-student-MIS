package com.mybooksystem.springboot.controller;

import com.wf.captcha.ChineseGifCaptcha;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.OutputStream;

/**
 * 生成验证码的前端控制器。
 *
 * 英文与数字验证码生成器。
 */
//@RestController
public class CodeCaptcha extends SpecCaptcha {

    /**
     * 验证码。
     */
    protected static String code = "";

    /**
     * 生成验证码。并回传到响应体中。
     * @param response 回传的响应体。
     * @throws Exception 出现的异常。
     */
//    @GetMapping("/getCode11")
//    @GetMapping("/code.jpg")
    public static void getCode(HttpServletResponse response) throws Exception {
        // 优先销毁，再创建。
        destroy();
//         核心，显示验证码的类。
        ServletOutputStream outputStream = response.getOutputStream();
//        ServletOutputStream outputStream1 = response.getOutputStream();


//        System.out.println(outputStream);
//        System.out.println("===========");
//        System.out.println(outputStream1);

        //算术验证码 数字加减乘除. 建议2位运算就行:captcha.setLen(2);
//        ArithmeticCaptcha captcha = new ArithmeticCaptcha(120, 40);

        // 中文验证码
//        ChineseCaptcha captcha = new ChineseCaptcha(120, 40);

        // 英文与数字验证码
        SpecCaptcha captcha = new SpecCaptcha(120, 40);

        //英文与数字动态验证码
//        GifCaptcha captcha = new GifCaptcha(120, 40);

        // 中文动态验证码
//        ChineseGifCaptcha captcha = new ChineseGifCaptcha(120, 40);
        // 几位数运算，默认是两位
//        captcha.setLen(2);
        captcha.setLen(4);
        // 获取运算的结果
        code = captcha.text().toLowerCase();
        // 生成验证码。
        captcha.out(outputStream);
//        System.out.println("当前验证码：" + code);
    }

    // 销毁验证码。
    public static void destroy(){
        ServletOutputStream outputStream = null;
        code = null;
//        System.out.println("code："+code);
    }
}