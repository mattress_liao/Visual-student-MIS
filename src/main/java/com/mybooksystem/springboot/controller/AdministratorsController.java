package com.mybooksystem.springboot.controller;

import com.mybooksystem.springboot.config.Result;
import com.mybooksystem.springboot.pojo.manage.Administrators;
import com.mybooksystem.springboot.service.AdministratorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 管理员信息获取相关的前端控制器。
 */
@RestController
public class AdministratorsController {

    @Autowired
    private AdministratorsService administratorsService;
    /**
     * 获取管理员信息
     */
    @GetMapping("/managementSystem/adminInfo/{username}")
    public Result<Administrators> getAdminInfo(@PathVariable("username") String username){
        System.out.println("当前用户名：" + username);
        Administrators administratorInfo = administratorsService.getAdministratorInfo(username);
        System.out.println("用户信息：" + administratorInfo);
        return new Result<>(0,"成功",administratorInfo);
    }

    /**
     * 修改管理员信息
     * @return
     *
     */
    @PutMapping("/managementSystem/adminInfo/{uid}")
    public Result<Boolean> updateAdminInfo(@PathVariable("uid") String uid,@RequestBody Administrators administrators){
//        System.out.println("uid = " + uid);
//        System.out.println("administrators = " + administrators);
//        System.out.println(administrators.getUid());
//        System.out.println(uid.equals(administrators.getUid().toString()));

        if (uid.equals(administrators.getUid().toString())){
//            System.out.println("执行了.........");
            if (administratorsService.updateAdminInfo(administrators)) {
                return new Result<>(0,"成功",true);
            }
        }
//        System.out.println("没有执行");
        return new Result<>(0,"失败",false);
    }
}
