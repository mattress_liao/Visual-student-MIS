package com.mybooksystem.springboot.controller.visualize;

import com.mybooksystem.springboot.config.Result;
import com.mybooksystem.springboot.pojo.visualize.classForOne.CoursesAndCredits;
import com.mybooksystem.springboot.pojo.visualize.classForOne.StudentDataFormClassForOne;
import com.mybooksystem.springboot.service.visualize.ClassForOneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 单个班级的可视化表格数据。
 */
@RestController
@RequestMapping("/managementSystem")
public class ClassForOneController {

    /**
     * service 层。
     */
    @Autowired
    private ClassForOneService classForOneService;

    /**
     * 根据传进来的 专业id 和 班级 id 进行获取学生数据。
     * @param proClassId：专业号 和 班级号。前2个时专业号，后面的是班级号。
     */
    @GetMapping("/classForOne/{proAndClassId}")
    public Result<Map<String,Object>> getData(@PathVariable("proAndClassId") String proClassId){

        // 根据 专业编号 获取 【科目名】列表
        List<CoursesAndCredits> courseList = classForOneService.getCourseListAndCredits(proClassId.substring(0, 2));

        System.out.println("classForOne ===》  " + proClassId);
        System.out.println("classForOne - 专业号 ===》  " + proClassId.substring(0,2));
        System.out.println("classForOne - 班级号 ===》  " + proClassId.substring(2));

        // 根据 专业编号 和 班级号 获取 【学号、姓名、性别、科目名、分数】列表
        List<StudentDataFormClassForOne> data = classForOneService.getData(proClassId);

        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("courseList",courseList);
        resultMap.put("data",data);

        courseList.forEach(System.out::println);
        System.out.println("----------------------------------");
        data.forEach(System.out::println);

        return new Result<>(200,"成功",resultMap);
    }

}
