package com.mybooksystem.springboot.controller.visualize;

import com.mybooksystem.springboot.config.Result;
import com.mybooksystem.springboot.pojo.visualize.classForAll.CoursesAndCredits;
import com.mybooksystem.springboot.pojo.visualize.classForAll.StudentDataFormClassForAll;
import com.mybooksystem.springboot.service.visualize.ClassForAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 单个专业的可视化表格数据。
 */
@RestController
@RequestMapping("/managementSystem")
public class ClassForAllController {

    /**
     * service 层。
     */
    @Autowired
    private ClassForAllService classForAllService;


    /**
     * 根据传进来的 专业id 进行获取学生数据。
     *
     * @param proId：需要获取的专业id.
     * @return
     *      返回获取到的结果集数据。
     */
    @GetMapping("/classForAll/{proId}")
    public Result<Map<String,Object>> getData(@PathVariable("proId") String proId){
        System.out.println("当前专业id为：" + proId);

        // 根据专业获取 数据。
        // 1. 根据 专业编号 获取 【科目名】列表
        List<CoursesAndCredits> courseList = classForAllService.getCourseListAndCredits(proId);

        // 根据 专业编号 获取 【学号、姓名、性别、班级、科目名、分数】列表
        List<StudentDataFormClassForAll> data = classForAllService.getData(proId);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("courseList",courseList);
        resultMap.put("data",data);

        courseList.forEach(System.out::println);
        System.out.println("----------------------------------");
        data.forEach(System.out::println);

        return new Result<>(200,"成功",resultMap);
    }
}
