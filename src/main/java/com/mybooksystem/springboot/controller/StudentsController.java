package com.mybooksystem.springboot.controller;

import com.mybooksystem.springboot.Excel导出.ChineseName;
import com.mybooksystem.springboot.config.Result;
import com.mybooksystem.springboot.pojo.StudentAndSubjectScore;
import com.mybooksystem.springboot.pojo.student.Students;
import com.mybooksystem.springboot.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 与学生相关的前端控制器。
 */
@RestController
@RequestMapping("/managementSystem")
public class StudentsController {

    Integer startNum ;

    @Autowired
    private StudentsService studentsService;

    List<Students> studentInfoList1;


    /**
     * 根据学号，获取对应的学生姓名。
     * @param studentId：需要查询的学生的学号。
     * @return
     *      返回查询到的学生姓名。
     *      查询不到返回 "null" [字符格式的null]
     */
    @GetMapping("/getStudentName/{studentId}")
    public String getStudentName(@PathVariable("studentId") String studentId){
        String studentName = studentsService.getStudentName(studentId);
        if (studentName!=null)
            return studentName;
        return "null";
    }

    /**
     * 查看该用户是否存在。
     *
     * @param studentId：要查询的学号。
     * @return
     *      true：查询到。
     *      false：查询不到。
     */
    @GetMapping("/isStudentInfo/{studentId}")
    public Boolean isStudentInfo(@PathVariable("studentId") String studentId){
        return studentsService.isStudentInfo(studentId);
    }

    @GetMapping("/getStudentsInfo")
    public Map<String,Object> getStudentsInfo(){

        Map<String, Object> maps = new LinkedHashMap<>();
        maps.put("state","200");
        maps.put("content","成功");
        //数据部分
        Map<String,Object> dataMaps = new LinkedHashMap<>();
        List<Map<String,Object>> data = new ArrayList<>();
        data.add(dataMaps);
        maps.put("data",data);





        //对应 ECharts00。
        Map<String,Integer> ECharts00 = new LinkedHashMap<>();
        ECharts00.put("studentNum",100);
        dataMaps.put("ECharts00", ECharts00);



        //对应 ECharts01。【访问量】
        Map<String,Integer> ECharts01 = new LinkedHashMap<>();
        ECharts01.put("studentNum",100);
        dataMaps.put("ECharts01", ECharts01);



        //对应 ECharts02。
        Map<String,Object> ECharts02 = new LinkedHashMap<>();
        //科目
        String[] scores = {"政治","物理","生物","历史","数学","化学","语文","英语","地理"};
        //各科平均分
//        Float[] avgScores = {68.0F,85.0F,60F,68F,86F,89F,69F,98F,95F};
        Double[] avgScores = {68.0D,85.0D,60D,68D,86D,89D,69D,98D,95D};
        // 各科平均再平均
        ECharts02.put("subjects",new ArrayList<>(Arrays.asList(scores)));
        List<Double> avgScoresList = new ArrayList<>(Arrays.asList(avgScores));
        ECharts02.put("avgScores",avgScoresList);
        Double avg = avgScoresList.stream().collect(Collectors.averagingDouble(Double::doubleValue));
        // 优化：只保留两位小数。
//        ECharts02.put("avgScoresAll",new ArrayList<>(Collections.singletonList(avg)));
        ECharts02.put("avgScoresAll",new DecimalFormat("#.00").format(avg));

        dataMaps.put("ECharts02", ECharts02);



        //对应 ECharts03。
        Map<String, Object> ECharts03 = new LinkedHashMap<>();
        // 成绩状态
        String[] scoresState = {"作弊","不及格","及格","良好","优秀","满分"};
        // 该状态下的人数
        Integer[] peoples = {100 ,150 ,200 ,250 ,300 ,350};
        ECharts03.put("year",2021);
        ECharts03.put("scoresStates",new ArrayList<>(Arrays.asList(scoresState)));
        ECharts03.put("peoples",new ArrayList<>(Arrays.asList(peoples)));
        dataMaps.put("ECharts03", ECharts03);



        //对应 ECharts04。【地图，】
        Map<String, Object> ECharts04 = new LinkedHashMap<>();
        //国内地区名称
        String[] region = {
                "南海诸岛", "北京", "天津", "上海", "重庆",
                "河北", "河南", "云南", "辽宁", "黑龙江",
                "湖南", "安徽", "山东", "新疆", "江苏",
                "浙江", "江西", "湖北", "广西", "甘肃",
                "山西", "内蒙古", "陕西", "吉林", "福建",
                "贵州", "广东", "青海", "西藏", "四川",
                "宁夏", "海南", "台湾", "香港", "澳门"};
        //各个地区的人数
        Integer[] regionPeoples = {
                0,0,0,0,0,
                0,0,0,0,0,
                0,0,0,0,0,
                0,0,0,0,0,
                0,0,0,0,0,
                0,100,0,0,0,
                0,0,0,0,0};
        List<Object> proportion = new ArrayList<>();
        // [100,50,0]
        // 最高比例
        proportion.add(100 );
        // 最小比例
        proportion.add(50);
        // 无比例
        proportion.add(0);

        ECharts04.put("region",region);
        ECharts04.put("regionPeoples",regionPeoples);
        ECharts04.put("proportion",proportion);
        dataMaps.put("ECharts04", ECharts04);



        //对应 ECharts05。
        Map<String, Object> ECharts05 = new LinkedHashMap<>();
        // 科目
        String[] subjectsForECharts05 = {"政治","物理","生物","历史","数学","化学","语文","英语","地理"};
        // 科目总人数
        Integer[] subjectsPeoples ={101,300,251,60,80,60,210,253,210};
        // 科目及格人数
        Integer[] subjectsPassPeoples = {80,210,251,60,40,55,180,233,209};
        ECharts05.put("subjects",new ArrayList<>(Arrays.asList(subjectsForECharts05)));
        ECharts05.put("subjectsPeoples",new ArrayList<>(Arrays.asList(subjectsPeoples)));
        ECharts05.put("subjectsPassPeoples",new ArrayList<>(Arrays.asList(subjectsPassPeoples)));
        dataMaps.put("ECharts05",ECharts05);



        //对应 ECharts06。
        Map<String, Object> ECharts06 = new LinkedHashMap<>();
        // 科目
        String[] subjectsForECharts06 = {"政治","物理","生物","历史","数学","化学","语文","英语","地理"};
        // 该科目的最高分
        Integer[] maxScores ={100,93,100,95,99,86,91,86,93};
        // 该科目的最低分
        Integer[] minScores = {59,60,65,51,49,60,55,64,60};
        ECharts06.put("subjects",new ArrayList<>(Arrays.asList(subjectsForECharts06)));
        ECharts06.put("minScores",new ArrayList<>(Arrays.asList(minScores)));
        ECharts06.put("maxScores",new ArrayList<>(Arrays.asList(maxScores)));
        dataMaps.put("ECharts06",ECharts06);



        //对应 ECharts07。
        Map<String, Object> ECharts07 = new LinkedHashMap<>();
        // 考勤状态
        String[] attendanceState ={"优秀","安全","良好","警示","严重警示"};
        // 考勤状态相对应的人数
        Integer[] peoplesForECharts07 = {30,20,50,1,0};
        ECharts07.put("className","19物联网应用技术B班");
        ECharts07.put("attendanceStates",new ArrayList<>(Arrays.asList(attendanceState)));
        ECharts07.put("peoples",new ArrayList<>(Arrays.asList(peoplesForECharts07)));
        dataMaps.put("ECharts07",ECharts07);



//        List<Map<String,Object>> lists = new ArrayList<>();
//
//        //科目与平均分数
//        Map<String,Float> mapForScoresAndScores = new HashMap<>();
//        mapForScoresAndScores.put("语文",68F);
//        mapForScoresAndScores.put("数学",80.5F);
//        mapForScoresAndScores.put("英语",70.5F);
//        mapForScoresAndScores.put("历史",68F);
//        mapForScoresAndScores.put("地理",98F);
//        mapForScoresAndScores.put("政治",78F);
//        mapForScoresAndScores.put("化学",65F);
//        mapForScoresAndScores.put("物理",59F);
//        mapForScoresAndScores.put("生物",87F);
//
//        //科目状态和人数
//        Map<String,Integer> mapForScoresStateAndPeoples = new HashMap<>();
//        //102 人
//        mapForScoresStateAndPeoples.put("作弊",1);
//        mapForScoresStateAndPeoples.put("不及格",3);
//        mapForScoresStateAndPeoples.put("及格",60);
//        mapForScoresStateAndPeoples.put("良好",25);
//        mapForScoresStateAndPeoples.put("优秀",10);
//        mapForScoresStateAndPeoples.put("满分",3);
//
//
//        //地区和地区人数
//        Map<String,Integer> mapForRegionAndRegionPeoples = new HashMap<>();
//
//
//        //科目数和科目总人数与该科目及格人数。【科目数可省略】
//        Map<String,List<Integer>> mapForSubjectsPeoplesAndSubjectsPassPeoples = new HashMap<>();
////        Map<String,Integer> mapForSubjectsPeoplesAndSubjectsPassPeoples = new HashMap<>();
////        mapForSubjectsPeoplesAndSubjectsPassPeoples.put("语文","");
//
//
//        HashMap<String, Object> map = new HashMap<>();
//
//        //对应ECharts00
//        map.put("studentNum",100);   //学生总人数。
//
//        //对应ECharts02
//        map.put("subjects",mapForScoresAndScores.keySet());   //科目数
//        map.put("AVGScores",mapForScoresAndScores.values());    //平均成绩成绩
//
////        mapForScoresAndScores.values().forEach(System.out::println);
//        Object[] values = mapForScoresAndScores.values().toArray();
//        System.out.println("values ==> " + values[0]);
//
//        map.put("AVGScoresAll",78);    //科目的平均分再平均
//
//        //对应ECharts03
//        map.put("subjectsState",mapForScoresStateAndPeoples.keySet());       //科目状态
//        map.put("peoples",mapForScoresStateAndPeoples.values());            //该状态的人数。
//
//        //对应ECharts04
////        map.put("",mapForRegionAndRegionPeoples.keySet()); //对应地区名
////        map.put("",mapForRegionAndRegionPeoples.values()); //对应地区人数
//
//        //对应ECharts05
////        map.put("subjects",mapForScoresAndScores.keySet()); //科目数
////        map.put("subjectsPeoples",mapForSubjectsPeoplesAndSubjectsPassPeoples.values());
////        map.put("subjectsPassPeoples",mapForSubjectsPeoplesAndSubjectsPassPeoples.values());
//
//
//
//        Map<String,Map<String,Float>> mapForSubjectsAndMinScoreAndMaxScore = new HashMap<>();
////        HashMap<String, Float> map1 = new HashMap<>();
//
////        mapForSubjectsAndMinScoreAndMaxScore.put("语文",map1);
//        String[] score = {"政治","物理","生物","历史","数学","化学","语文","英语","地理"};
//
//        HashMap<String, Float> map1;
//        for (int i = 0; i < 9; i++) {
//            map1 = new HashMap<>();
//            map1.put("min", (float) ((Math.random()*50) + 0));  //0-49
//            map1.put("max",(float) ((Math.random()*50) + 50));  //50-100
//            mapForSubjectsAndMinScoreAndMaxScore.put(score[i],map1);
//        }
//        maps.put("comparison",mapForSubjectsAndMinScoreAndMaxScore);
//        //对应ECharts06
////        map.put("minScore","");
////        map.put("maxScore","");
//        lists.add(map);
//
//        maps.put("data",lists);

        return maps;
    }

    /**
     * 获取 前 n 条学生数据。
     *
     * 注：
     *      layUI的重载就是重新发送请求。
     *
     * @param endNum 最后的条数。
     *
     * @return
     *      查询到的前 n 条学生信息数据记录。
     */
    @GetMapping("/studentInfoList/{classId}/{endNum}")
    public Result<List<Students>> studentInfoList(
            @PathVariable("endNum") Integer endNum,
            @PathVariable("classId") String classId,
            @RequestParam(value = "studentId",required = false) String studentId,            /*学号*/
            @RequestParam(value = "idCard",required = false) String idCard,                  /*身份证号*/
            @RequestParam(value = "admissionType",required = false) String admissionTypeStr    /*入学类型*/

    ){
        System.out.println("执行了！ studentInfoList!!!");
        // 判断是搜索情况。
        if (studentId!=null || idCard!=null || admissionTypeStr!=null){
            return studentInfoListFromSearch(classId,studentId,idCard,admissionTypeStr);
        }
        // 1. 从数据库中获取数据。
        // 2. 从数据库中获取前100条学生的记录。
        this.studentInfoList1 = new ArrayList<>(studentsService.getStudentInfoList(classId,100));
        System.out.println("执行了！ studentInfoList 【获取前100条学生信息】");
        studentInfoList1.forEach(System.out::println);
        // 3. 回传数据。
        return new Result<>(0,this.studentInfoList1.size(),"成功",this.studentInfoList1);
    }


    /**
     * 全局搜索函数。【不用指定班级】。
     *
     * 按照指定参数进行查询学生信息。
     * @return
     */
    @PostMapping("/studentInfoList/{classId}")
//    @PostMapping("/studentInfoList/")
    public Result<List<Students>> studentInfoListFromSearch(
            @PathVariable("classId") String classId,
            @RequestParam(value = "studentId",required = false) String studentId,            /*学号*/
            @RequestParam(value = "idCard",required = false) String idCard,                  /*身份证号*/
            @RequestParam(value = "admissionType",required = false) String admissionTypeStr    /*入学类型*/
            ){
        System.out.println("studentId ==》 " + studentId);
        System.out.println("idCard ==》 " + idCard);
        System.out.println("admissionType ==》 " + admissionTypeStr);

        // 入学类型.
        // 请选择：-1。
        // 高考：0.
        // 学考：1.
        // 3+2：2.
        // 成考：3。
        String[] admissionTypeArray = {"请选择","高考","学考","3+2","成考"};
        // 就读类型。
        // 请选择：-1.
        // 全日制：0.
        // 非全日制：1。
//        String[] studyTypeArray = {"请选择","全日制","非全日制"};

        // 判断全部是空的情况。
        if (
            (studentId==null         || "".equals(studentId))           &&
            (idCard==null            || "".equals(idCard))              &&
            (admissionTypeStr==null  || "".equals(admissionTypeStr))
        ){
            if (this.studentInfoList1 == null){
                this.studentInfoList1 = new ArrayList<>(studentsService.getStudentInfoList(classId,100));
            }
            return new Result<>(0,this.studentInfoList1.size(),"没有选择查询条件",this.studentInfoList1);
        }

        Integer admissionType = null;

        // 进行数字转字符，并剔除【请选择】这个选择。
        try {
            admissionType = Integer.parseInt(admissionTypeStr);
            if (admissionTypeArray[0].equals(admissionTypeArray[admissionType+1])){
                return new Result<>(0,this.studentInfoList1.size(),"没有选择查询条件",this.studentInfoList1);
            }
        } catch (NumberFormatException e) {
            System.out.println("admissionType 转换失败！");
        }


        // 所有学生的基本信息。
        List<Students> allStudentsInfo = new ArrayList<>(studentsService.getAllStudentInfoList());
        // 获取指定学号的学生信息。（学号是唯一的）
//        System.out.println(allStudentsInfo.get(0).getStudentId());

        List<Students> list = new ArrayList<>();
        for (Students students : allStudentsInfo) {
            // 学号查询。【学号和身份证是唯一id，无序继续查询】
            if (students.getStudentId().equals(studentId)) {
                System.out.println("查询到该学生（学号查询）：" + students);
                list.add(students);
                return new Result<>(0,1,"成功",list);
            }
            // 身份证号查询。
            if (students.getIdCard().equals(idCard)) {
                System.out.println("查询到该学生（身份证查询）：" + students);
                list.add(students);
                return new Result<>(0,1,"成功",list);
            }

            // 入学类型查询
            if (admissionType!=null && students.getAdmissionType().equals(admissionTypeArray[admissionType+1])){
                System.out.println("查询到该学生（入学类型查询）：" + students);
                list.add(students);
            }
        }
        // 获取指定身份证的学生信息。（身份证唯一）
//        System.out.println(allStudentsInfo.get(0).getIDCard());
//
//        // 入学类型
//        System.out.println(allStudentsInfo.get(0).getAdmissionType());
        System.out.println("=================================");
        if (list!=null){
            list.forEach(System.out::println);
        }
        // bug 找不到数据会在一直转。【已解决】
        return new Result<>(0,list.size(),"成功",list);
    }

    /**
     * 入学类型。
     * @return
     */
    @GetMapping("/getAdmissionType")
    public Map<String,Object> getAdmissionType(){
        Map<String, Object> admissionTypeMaps = new LinkedHashMap<>();
        admissionTypeMaps.put("code",0);
        admissionTypeMaps.put("msg","成功");
        //data
        List<Map<String,Object>> admissionTypeLists = new ArrayList<>();
        Map<String,Object> admissionTypes = new LinkedHashMap<>();
        // 入学类型。【-1,0,1,2,3】
        String[] admissionType = {"请选择","高考","学考","3+2","成考"};

        for (int i = 0; i < admissionType.length; i++) {
            admissionTypes = new LinkedHashMap<>();
            admissionTypes.put("id",i-1);          // id
//            admissionTypes.put("name",admissionType[i]);    // 名称
            admissionTypes.put("name",admissionType[i]);    // 名称
            admissionTypes.put("parentId",0);    // 不知道是啥
            admissionTypeLists.add(admissionTypes);
        }

        admissionTypeMaps.put("data",admissionTypeLists);


        return admissionTypeMaps;
    }

    /**
     * 获取表格表头信息。
     */
    @GetMapping("/getScoreList/{classId}")
    public Result<String> getScoreList(@PathVariable("classId") String classId){
        // 表格表头。
//         String[] title = {"学号","姓名","班级","语文","数学","英语","历史","地理","政治","化学","物理"};
         List<String> title = new ArrayList<>();
         title.add("学号");
         title.add("姓名");
         title.add("班级");

         // 当查询不到页面时添加到页面上，会发生异常。
        try {
//            title.addAll(studentsService.getCourseNameList(String.valueOf(4544555)));
//            title.addAll(studentsService.getCourseNameList(String.valueOf(24)));
            title.addAll(studentsService.getCourseNameList(classId));
            title.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String titleList = Arrays.toString(title);
//        return new Result<>(200,"成功",titleList);
//        System.out.println("转换：" + title.toString());
        return new Result<>(200,"成功",title.toString());
    }

    /**
     * 获取该班级的学生与各科目分数的列表。
     *
     * 学生成绩管理表，获取数据部分。
     * @return
     */
    @GetMapping("/getStudentAndSubjectScoreList/{classId}/{selectNum}")
    public Result<List<StudentAndSubjectScore>> getStudentAndSubjectScoreList(@PathVariable("classId") String classId,@PathVariable(value = "selectNum",required = false) String selectNum){
        // 表格数据部分。
        // 默认查询前100个学生的成绩信息表。
        // 查询完，封装到一个list对象中。
        List<StudentAndSubjectScore> studentInfoListForScore = studentsService.getStudentInfoListForScore(classId,100);
        studentInfoListForScore.forEach(System.out::println);
        System.out.println("\n\n\n");
        // 必须返回 0 才可展示数据表格。
        return new Result<>(0, studentInfoListForScore.size(),"成功",studentInfoListForScore);
    }

    /**
     * 获取就读状态
     * @return
     */
    @Deprecated
//    @GetMapping("/getStudyType")
    public Map<String,Object> getStudyType(){
        Map<String, Object> admissionTypeMaps = new LinkedHashMap<>();
        admissionTypeMaps.put("code",0);
        admissionTypeMaps.put("msg","成功");
        //data
        List<Map<String,Object>> admissionTypeLists = new ArrayList<>();
        Map<String,Object> admissionTypes = new LinkedHashMap<>();
        // 就读类型。【-1,0,1】
        String[] admissionType = {"请选择","全日制","非全日制"};

        for (int i = 0; i < admissionType.length; i++) {
            admissionTypes = new LinkedHashMap<>();
            admissionTypes.put("id",i-1);          // id
//            admissionTypes.put("name",admissionType[i]);    // 名称
            admissionTypes.put("name",admissionType[i]);    // 名称
            admissionTypes.put("parentId",0);    // 不知道是啥
            admissionTypeLists.add(admissionTypes);
        }

        admissionTypeMaps.put("data",admissionTypeLists);


        return admissionTypeMaps;
    }






    /************************************** 【学生信息 crud 】 *****************************************************/

    /**
     * 添加学生信息。
     */
    @PostMapping("/studentInfo")
    public Result<Object> studentInfo(@RequestBody Students students){
        System.out.println("students ==> " + students);
        if (students!=null){

             // 需要进行转换。

//            students = new Students("1924010223","赵七","454241212021021202",1,
////                "2021-09-18"
//                    new Date()
//                    ,"高考","全日制","本科",4,
//                    "党员","广东工程职业技术学院");
//            Boolean isAdd = studentsService.addStudentInfo(students);
            try {
                return new Result<>(0, 1, "成功", studentsService.addStudentInfo(students));
            } catch (Exception e) {
                System.out.println("数据添加失败！");
                e.printStackTrace();
                return new Result<>(499,0,"添加失败！","");
            }
        }
        return new Result<>(499,0,"学生信息为 null！","");
    }

    /**
     * 删除学生信息。
     * @param stuId
     * @return
     */
    @DeleteMapping("/deleteStudentInfo/{stuId}")
    public Result<String> deleteStudentInfo(@PathVariable("stuId") String stuId){
        System.out.println("删除学号为" + stuId + "的学生！");
        studentsService.deleteStudentInfo(stuId);
        return new Result<>(0,1,"成功",String.valueOf(0));
    }

    /**
     * 根据学号修改学生信息。
     *
     * 注意：
     *      在 PUT 请求中，需要获取从前端传递过来的数据，需要使用 @PathVariable + @RequestBody.
     *
     *
     * @param studentId：要进行修改的学号。
//     * @param students：修改的学生信息。
     * @return
     *      修改结果。
     */
    @PutMapping("/studentInfo/{studentId}")
    public Result<Object> studentInfo(@RequestBody Students students,@PathVariable("studentId") String studentId){
        System.out.println("studentId ==》 "+ studentId);
//        System.out.println("studentName ==》 "+ studentName);
        System.out.println("students ==》 "+ students);
        if (!studentId.equalsIgnoreCase(students.getStudentId())){
            return new Result<>(200,0,"请别修改内容！","");
        }
        Boolean isOk = studentsService.updateStudentInfo(students);
        // 修改成功
        return new Result<>(200,0,"成功",isOk);
    }

    /**
     * 根据学号查询该学生信息，
     * @return
     *      返回查询到的学生信息，
     *      查询不到返回null。
     */
    @GetMapping("/studentInfo/{studentId}")
    public Result<Students> studentInfo(@PathVariable("studentId") String studentId){
        System.out.println("学号：" + studentId);
        // 如果学号存在。
        if (isStudentInfo(studentId)) {
            Students studentInfo = studentsService.getStudentInfo(studentId);
            System.out.println("studentInfo = " + studentInfo);
            return new Result<>(200,0,"成功",studentInfo);
        }
        return new Result<>(499,0,"学生信息为 null！",null);
    }







    /************************************** 【学生成绩 crud 】 *****************************************************/

    /**
     * 添加学生成绩表。
     * @return
     */
    @PostMapping("/studentScoreInfo")
    public Result<Object> addStudentScoreInfo(@RequestBody(required = false) StudentAndSubjectScore studentScore){
        System.out.println("学生成绩表：" + studentScore);
        try {
// sh数据合法检验。
            List<Object> list = studentScore.toList();
            list.forEach(System.out::println);

            for (int i = 0; i < list.size(); i++) {
                if ("".equals(list.get(i))){
                    System.out.println("第"+i+"个元素为 空！");
                    return new Result<>(0,"第"+i+"个元素不能为空",null);
                }
                if (list.get(i) == null){
                    System.out.println("第"+i+"个元素为 null！");
                    return new Result<>(0,"第"+i+"个元素为null",null);
                }
            }

            System.out.println("数据合法，进行数据库操作！");
            studentsService.addStudentScoreInfo(studentScore);
            // 若数据合法。
            return new Result<>(0,"成功","成功");
        }catch (NullPointerException nullError){
            return new Result<>(0,"失败, 科目列表为 null。",null);
        }
    }

    /**
     * 根据id删除学生成绩表。
     * @param stuId：要删除的学生学号
     * @return
     *
     */
//    @DeleteMapping("/studentScoreInfo/{stuId}")
    public Result<StudentAndSubjectScore> deleteStudentScoreInfo(@PathVariable("stuId") String stuId){
        return new Result<>(0,"",null);
    }

    /**
     * 根据学号进行 学生成绩表的修改。
     * @param studentScore：学生成绩信息。
     * @param studentId：学生学号
     * @return
     *      返回结果集。
     */
    @PutMapping("/studentScoreInfo/{classId}/{stuId}")
    public Result<Object> updateStudentScoreInfo(
            @RequestBody StudentAndSubjectScore studentScore,
            @PathVariable("stuId") String studentId,
            @PathVariable("classId") String classId
    ){
        System.out.println("studentScore = " + studentScore);
        System.out.println("studentId = " + studentId);

        // sh数据合法检验。
        List<Object> list = studentScore.toList();
        list.forEach(System.out::println);

        for (int i = 0; i < list.size(); i++) {
            if ("".equals(list.get(i))){
                System.out.println("第"+i+"个元素为 空！");
                return new Result<>(0,"第"+i+"个元素不能为空",null);
            }
            if (list.get(i) == null){
                System.out.println("第"+i+"个元素为 null！");
                return new Result<>(0,"第"+i+"个元素为null",null);
            }
        }

        System.out.println("数据合法，进行数据库操作！");
        // 若数据合法。
        studentsService.updateStudentScoreInfo(classId,studentScore);

        // 如果全部数据不为空。

        return new Result<>(0,"成功",null);
    }

    /**
     * 根据学号进行学生成绩查询。
     * @param studentId：要查询的学号。
     * @return
     *      查询到的学生成绩信息。
     *      查询不到返回 null。
     */
    @GetMapping("/studentScoreInfo/{stuId}")
    @Deprecated
    public Result<StudentAndSubjectScore> getStudentScoreInfo(@PathVariable("stuId") String studentId){
        return new Result<>(0,"",null);
    }



}