package com.mybooksystem.springboot.controller;

import com.mybooksystem.springboot.pojo.ClassState;
import com.mybooksystem.springboot.pojo.student.Students;
import com.mybooksystem.springboot.service.ClassStateService;
import com.mybooksystem.springboot.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户状态的控制器
 *
 * 包括 登录、注册
 */
@RestController
@Deprecated
public class UserStateController {

    /**
     * 班级状态
     */
    @Autowired
    private ClassStateService classStateService;

    /**
     * 学生信息表
     */
    @Autowired
    private StudentsService studentsService;

    /**
     * 获取班级状态：（班级总人数、班级全科目及格人数、及格率、科目总数）
     */
    @GetMapping("/classState")
    @Deprecated
    public ClassState classState(){
        ClassState classState = new ClassState(
                classStateService.findClassNum(),
                classStateService.findClassPassNum(),
                classStateService.findClassPassPercentage(),
                classStateService.findSubjectsNumbers()
        );
        System.out.println("classState = " + classState);
        return classState;
    }

    /**
     *
     * @return
     */
    @GetMapping("/getStudentsTab")
    @Deprecated
    public List<Students> getStudentsTabNoPath(){
        return studentsService.getStudentInfoList();
    }

    /**
     * 获取学生的信息展示表格。
     * @param number
     * @return
     */
    @GetMapping("/getStudentsTab/{number}")
    @Deprecated
    public List<Students> getStudentsTab(
            @PathVariable(value = "number") Integer number
    ){
        System.out.println("getStudentsTab 该函数执行了！。。。。。。");
        System.out.println("当前要显示的条数(默认是10条): " + number);
        return studentsService.getStudentInfoList();
    }

    /**
     * 分页信息
     */
    public List<Object> getPagination(){
        return null;
    }

}
