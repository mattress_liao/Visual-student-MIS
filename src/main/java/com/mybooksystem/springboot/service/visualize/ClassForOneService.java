package com.mybooksystem.springboot.service.visualize;

import com.mybooksystem.springboot.Dao.visualize.ClassForOneMapper;
import com.mybooksystem.springboot.pojo.visualize.classForOne.CoursesAndCredits;
import com.mybooksystem.springboot.pojo.visualize.classForOne.StudentDataFormClassForOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 单个班级的可视化表格数据 [service 层]
 */
@Service
public class ClassForOneService {

    @Autowired
    private ClassForOneMapper classForOneMapper;

    /**
     * 根据 专业编号 和 班级号 获取 【学号、姓名、性别、科目名、分数】列表。
     * @param proClassId：需要获取的 专业号 和 班级号。
     */
    public List<StudentDataFormClassForOne> getData(String proClassId) {
        // 24190102 ==》 24  190102
        // substring 下标从 0 开始。
        System.out.println("获取的专业");
       return classForOneMapper.getData(proClassId.substring(0,2),proClassId.substring(2));
    }

    /**
     * 根据 专业编号 获取 【科目名】列表。
     * @param proId：专业号
     */
    public List<CoursesAndCredits> getCourseListAndCredits(String proId) {
        return classForOneMapper.getCourseListAndCredits(proId);
    }
}
