package com.mybooksystem.springboot.service.visualize;

import com.mybooksystem.springboot.Dao.visualize.ClassForAllMapper;
import com.mybooksystem.springboot.pojo.visualize.classForAll.CoursesAndCredits;
import com.mybooksystem.springboot.pojo.visualize.classForAll.StudentDataFormClassForAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 单个专业的可视化表格数据 [service 层]
 */
@Service
public class ClassForAllService {

    /**
     * mapper 层。
     */
    @Autowired
    private ClassForAllMapper classForAllMapper;

    /**
     * 根据 专业编号 获取 【科目名】列表。
     * @param proId：专业号。
     * @return
     *      返回结果集。
     */
    public List<CoursesAndCredits> getCourseListAndCredits(String proId) {
        return classForAllMapper.getCourseListAndCredits(proId);
    }

    /**
     * 根据 专业编号 获取 【学号、姓名、性别、科目名、分数】列表。
     * @param proId：专业编号
     * @return
     *      返回结果集。
     */
    public List<StudentDataFormClassForAll> getData(String proId) {
        return classForAllMapper.getData(proId);
    }
}
