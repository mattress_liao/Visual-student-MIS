package com.mybooksystem.springboot.service;


import com.mybooksystem.springboot.Dao.LoginDao;
import com.mybooksystem.springboot.pojo.User;
import com.mybooksystem.springboot.util.DesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 登录判断
 */
@Service
public class LoginService extends BaseService{

    @Autowired
    private LoginDao loginDao;

    /**
     * 查看用户是否存在
     * @param username
     * @return
     */
    public Boolean isUsername( String username) {
        if (    username!=null  &&  !username.equals("")  && loginDao.isUsername(username) != null  ){
            System.out.println("用户名：" +username);
            System.out.println("判断结果："  + loginDao.isUsername(username).equals(username));

            return loginDao.isUsername(username).equals(username);
        }
        return false;
    }

    /**
     * 查询用户名，密码是否正确。
     *
     * @param username 用户名
     * @param password 密码
     * @return
     *      存在   则返回    true。
     *      不存在 则返回     false。
     */
    public Boolean loginState( String username, String password) {
        User user = null;
        String encrypt = null;
        try {
            user = loginDao.loginState(username);
            encrypt = des.decrypt(user.getPassword());
            System.out.println("-------------------- loginState --------------------");
            System.out.println("【解密】前 的数据：" + user.getPassword());
            System.out.println("【解密】后 的数据：" + encrypt);
            return user != null && encrypt.equals(password);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
