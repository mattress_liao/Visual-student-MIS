package com.mybooksystem.springboot.service;

import com.mybooksystem.springboot.Dao.AdministratorsMapper;
import com.mybooksystem.springboot.controller.BaseController;
import com.mybooksystem.springboot.pojo.manage.Administrators;
import com.mybooksystem.springboot.util.DesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 管理员相关的 service 层。
 */
@Service
public class AdministratorsService extends BaseService {
    @Autowired
    private AdministratorsMapper administratorsMapper;

    /**
     * 获取管理员信息
     * @param username
     * @return
     */
    public Administrators getAdministratorInfo(String username) {
//        return administratorsMapper.getAdministratorInfo(username);
        Administrators administratorInfo = administratorsMapper.getAdministratorInfo(username);
        // 解密 --- 未完成。
        try {
//            des = new DesUtils(desKey);//自定义密钥
            System.out.println("-------------------- getAdministratorInfo --------------------");
            System.out.println("【解密】前 的密码 = " + administratorInfo.getPassword() );
            System.out.println("【解密】后 的密码 = " + des.decrypt(administratorInfo.getPassword()));

            administratorInfo.setPassword(des.decrypt(administratorInfo.getPassword()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
//        administratorInfo.setPassword(administratorInfo.getPassword());
        return administratorInfo;
    }

    /**
     * 修改管理员信息
     * @param administrators
     * @return
     */
    public Boolean updateAdminInfo(Administrators administrators){


        try {
//            DesUtils des = new DesUtils(desKey);//自定义密钥
            System.out.println("-------------------- updateAdminInfo --------------------");
            System.out.println("【加密】前 的密码：" + administrators.getPassword());
            System.out.println("【加密】后 的密码：" + des.encrypt(administrators.getPassword()));

            administrators.setPassword(des.encrypt(administrators.getPassword()));
            System.out.println("管理员信息：" + administrators);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return administratorsMapper.updateAdminInfo(administrators);
    }
}
