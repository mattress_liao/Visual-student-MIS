package com.mybooksystem.springboot.service;

import com.mybooksystem.springboot.util.DesUtils;

public class BaseService {

    /**
     * 加解密秘钥。
     */
    private static final String desKey = "user";

    static DesUtils des;//自定义密钥

    static {
        try {
            des = new DesUtils(desKey);
        } catch (Exception e) {
            e.printStackTrace();
            des = null;
        }
    }
}
