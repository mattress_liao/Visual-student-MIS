package com.mybooksystem.springboot.service;

import com.mybooksystem.springboot.Dao.ClassStateDao;
import com.mybooksystem.springboot.pojo.ClassState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 班级状态 service 层
 */
@Service
public class ClassStateService {


    private final ClassState classState = new ClassState();

    @Autowired
    private ClassStateDao classStateDao;
    /**
     * 查询班级总人数
     * @return 班级总人数。
     */
    public Integer findClassNum() {
        classState.setClassNum( classStateDao.findClassNum() );
        return classState.getClassNum();
    }

    /**
     * 班级全科目及格人数
     * @return
     */
    public Integer findClassPassNum() {
        /**
         * bug，查询时会丢失最后一条数据
         */
        classState.setClassPassNum( classStateDao.findClassPassNum());
        return classState.getClassPassNum();
    }

    /**
     * 及格率
     * @return
     */
    public Float findClassPassPercentage() {
//        return classStateDao.findClassPassPercentage();

//        System.out.println("班级全科及格人数：" + (classState.getClassPassNum() *10000));
//        System.out.println("及格率（成百倍）：" + (classState.getClassPassNum() *10000) / classState.getClassNum());
//        System.out.println("及格率：" + (classState.getClassPassNum() *10000) / classState.getClassNum()
//                / 100.0F);
        //百分比计算公式 及格人数/总人数。（下面是计算机中的计算方式）
        return (classState.getClassPassNum() *10000) / classState.getClassNum()
                / 100.0F;
    }

    /**
     * 科目总数
     * @return
     */
    public Integer findSubjectsNumbers() {
        return classStateDao.findSubjectsNumbers();
    }
}
