package com.mybooksystem.springboot.service;

import com.mybooksystem.springboot.Dao.SecondCollegeMapper;
import com.mybooksystem.springboot.pojo.subjects.SecondCollegeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 二级学院信息的 service层
 */
@Service
public class SecondCollegeService {

    /**
     * 二级学院信息的 mapper层。
     */
    @Autowired
    private SecondCollegeMapper secondCollegeMapper;

    public List<SecondCollegeInfo> getSecondCollegeAndProClassSituationInfo() {
        return secondCollegeMapper.getSecondCollegeAndProClassSituationInfo();
    }
}
