package com.mybooksystem.springboot.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * layUI下拉菜单格式化类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Deprecated
public class LayUiDropdown {
    /**
     * 标题名称
     */
    private String title;
    /**
     * 编号
     */
    private String id;
    /**
     * 子标签
     */
    private LayUiDropdown[] child;
}
