package com.mybooksystem.springboot.config;

import com.mybooksystem.springboot.interceptor.EncodingInterceptor;
import com.mybooksystem.springboot.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// java配置类

/**
 * web配置页面。
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
//public class WebConfig {
    @Autowired
    private LoginInterceptor loginInterceptor;

    @Autowired
    private EncodingInterceptor encodingInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        // 添加拦截器。
        registry.addInterceptor(loginInterceptor).addPathPatterns("/manageSystem/**","/managementSystem/**","/getStudentsDataPrefix")
//                .excludePathPatterns("/register.html","/manageSystem/signIn.html","/css/**","/js/**","/layui/**");
                .excludePathPatterns("/register.html","/manageSystem/signIn.html");
//                .excludePathPatterns("/","/index","/index.html","/login");

        // 字符编码拦截器
        registry.addInterceptor(encodingInterceptor).addPathPatterns("/**");
    }
}


