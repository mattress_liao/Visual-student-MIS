package com.mybooksystem.springboot.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * 回传的结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//@ToString
public class Result<T> {
//public class Result {

    /**
     * 页面状态码
     */
    private Integer code;
    /**
     * 数据条数
     */
    private Integer count;
    /**
     * 内容
     */
    private String msg;
    /**
     * 回传的数据
     */
//    private List<Map<String,Object>> data;
    private T data;

    /**
     * 信息错误情况！
     * @param code
     * @param msg
     */
    public Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 非用户查询的情况，不需要回传条数。
     * @param code
     * @param msg
     * @param data
     */
//    public Result(Integer code, String msg, List<Map<String,Object>> data) {
    public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", count=" + count +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
