package com.mybooksystem.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 该配置类解决跨域问题。
 */
@Configuration
public class CrosConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
//                .allowedOrigins("*")      //使用 allowedHeaders("*") 时，不能使用  allowedOrigins("*")。
//                .allowedOriginPatterns("*")
                .allowedOrigins("*")
                .allowedMethods("GET","HEAD","POST","PUT","DELETE","OPTIONS")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");

        /**
         * 注意：
         *  allowedOriginPatterns()方法不要用allowedOrigins()，不然项目运行时请求后端接口会报错。
         *  大概意思就是allowCredentials为true的时候allowedOrigins不能为*。
         */
    }
}
