package com.mybooksystem.springboot.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 存放 Result 类的data属性的类。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValueData<T> {
    /**
     * 存放键
     */
    private String key;
    /**
     * 存放值
     */
    private T value;

}
