package com.mybooksystem.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybooksystemApplication {

    public static void main(String[] args) {
//        new ChineseName();  //随机生成中文名字
        SpringApplication.run(MybooksystemApplication.class, args);
    }

}
