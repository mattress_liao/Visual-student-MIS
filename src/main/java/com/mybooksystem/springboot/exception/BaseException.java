package com.mybooksystem.springboot.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 区别：
 *      1.注解@ControllerAdvice方式只能处理控制器抛出的异常。此时请求已经进入控制器中。
 *      2.类ErrorController方式可以处理所有的异常，包括未进入控制器的错误，比如404,401等错误
 *      3.如果应用中两者共同存在，则@ControllerAdvice方式处理控制器抛出的异常，类ErrorController方式未进入控制器的异常。
 *      4.@ControllerAdvice方式可以定义多个拦截方法，拦截不同的异常类，并且可以获取抛出的异常信息，自由度更大。
 */
@RestControllerAdvice
public class BaseException {

    @ExceptionHandler(NullPointerException.class)
    public String nullPointerException(){
        return "对象为 null！";
    }

    @ExceptionHandler(ArrayIndexOutOfBoundsException.class)
    public String arrayIndexOutOfBoundsException(){
        return "数组越界！";
    }
    @ExceptionHandler(ArithmeticException.class)
    public String ArithmeticException(HttpServletRequest request, HttpServletResponse response){
//        try {
////            response.sendRedirect("/error/500.html");
//            request.getRequestDispatcher("/error/5xx.html").forward(request,response);
//
//        } catch (IOException | ServletException e) {
//            e.printStackTrace();
//        }
        return "算术异常！";
    }
}
