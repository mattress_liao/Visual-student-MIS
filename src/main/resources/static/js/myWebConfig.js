// var ip = "120.24.169.207"
var ip = "localhost"
var port = "8080"
var studentsDataPrefix = ""

var webUrl = "http://"+ip+":"+port

// 全局对象。学生信息数据对象共享变量【单个】。
var studentData = {}

// 全局对象。管理员数据对象共享变量。
var userData = {}

/**
 * 处理响应头中的 cookie 属性。
 *
 * bug：多个值切割，会出现 key 第一个字符是空空格的情况
 *
 * @return
 *      返回找到并且成功转换的编码。
 */
function cookieDecodeForValue(key) {
    // 获取请求头中的某个信息。
    var req = new XMLHttpRequest();
    req.open('GET', document.location, false);
    req.send(null);
    var headers = req.getAllResponseHeaders().toLowerCase(); // 获取所有请求头信息。
    // headers = req.getResponseHeader('Set-Cookie');    // 获取请求头中的某个信息。
    headers = req.getResponseHeader('Cookie');    // 获取请求头中的某个信息。
    // console.log("请求头信息：")
    // console.log(headers)

    /********* cookie 解码 **********/
        //定义变量储存cookie
        // let strCookie=document.cookie;
    let strCookie=headers;
    //将多cookie切割为多个名/值对
    // let arrCookie=strCookie.split("; ");
    let arrCookie=strCookie.split(";"); // 多个值切割，会出现 key 第一个字符是空空格的情况

    let user;
    //遍历cookie数组，处理每个cookie对
    for(let i=0;i<arrCookie.length;i++){
        let arr=arrCookie[i].split("=");
        //找到名称为userId的cookie，并返回它的值
        if("username"==arr[0]){
            // if(key===arr[0]){
            user=arr[1];
            break;
        }
    }
    // 得到cookie的值。
    // console.log("==========================")
    // console.log("cookie切片：")
    // console.log(user)
    // console.log("==========================")
    // // cookie 解码。
    // console.log("解码：")
    // console.log(decodeURIComponent(user));
    return decodeURIComponent(user)
}


// 获取最新位置的学生数据的前缀路径。
$.ajaxSettings.async = false;   //取消异步执行
$.getJSON(webUrl+"/getStudentsDataPrefix",function (data) {
    // console.log("获取学生数据的前缀：")
    // console.log(data)
    // console.log(data.data)
    studentsDataPrefix = data.data
})

// 在每个需要调用的函数中需要恢复同步。